1. Open terminal

2. `git clone https://gitlab.com/mfrydr/eslintrctest.git`

3. `cd eslintrctest`

4. `eslint .` should report the following problems:

```
$ eslint .

D:\maja\temp\eslintrctest\01_js_review\script.js
  1:1   warning  Use the global form of 'use strict'  strict
  6:41  error    Strings must use doublequote         quotes

✖ 2 problems (1 error, 1 warning)
  1 error and 0 warnings potentially fixable with the `--fix` option.
```

5. Open VSCode. Open the `eslintrctest` directory in VSCode

6. View > Problems should show the same problems as above in `script.js` (but currently in student lab accounts it does not). 
   Once the issue is fixed, it might take a few seconds for the problems to show and you might have to type something and save `script.js` to
   trigger ESLint.

7. Ctrl + Shift + P and search for ESLint: Show Ouptut Channel -- this should show no errors in the ESLint server startup but currently on a student account it shows permission errors like:

```
 like “Error: EPERM: operation not permitted, realpath 'D:\maja\temp\eslintrctest\01_js_review\script.js'”
```
